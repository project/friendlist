<?php

function friendlist_ui_overview_relation_types(){

  // Get the list of items
  friendlist_api_load_all_relation_types($return);

  foreach ($return as $rt) {
    # $form[ $rt['rtid'] ]['#rt'] = $rt; # No idea what this is for
    $form[ $rt['rtid'] ]['name'] = array('#value' => check_plain($rt['name']));
    $form[ $rt['rtid'] ]['name_p'] = array('#value' => check_plain($rt['name_p']));
    $form[ $rt['rtid'] ]['type'] = array('#value' => $rt['oneway'] ? t('One way') : t('Reciprocal')   );
    $form[ $rt['rtid'] ]['active'] = array('#value' => $rt['active'] ? t('Yes') : t('No')   );
    $form[ $rt['rtid'] ]['edit'] = array('#value' => l(t('edit'), "admin/settings/friendlist_relation_types/edit/".$rt['rtid'] ));
  }
  return $form;

}


function theme_friendlist_ui_overview_relation_types($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['name'])) {
      $rt = &$form[$key];

      $row = array();
      $row[] = drupal_render($rt['name']);
      $row[] = drupal_render($rt['name_p']);
      $row[] = drupal_render($rt['type']);
      $row[] = drupal_render($rt['active']);
      $row[] = drupal_render($rt['edit']);
      $rows[] = array('data' => $row);
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No relations available.'), 'colspan' => '5'));
  }

  $header = array(t('Name'), t('Name plur'), t('Type'), t('Active') );
  $header[] = array('data' => t('Operations'), 'colspan' => '2');
  return theme('table', $header, $rows, array('id' => 'taxonomy')) . drupal_render($form);
}




function friendlist_ui_form_relation_type(&$form_state, $edit=NULL ){

  // If $edit is not set, it's a new form. Set a state (used during submit to
  // know if it's a new record)
  if(!$edit){
    $edit = array(
      'name' => '',
      'name_p' => '',
      'oneway' => 0,
      'active' => 1,
    );
    $form_state['is_new']=TRUE;
  } else {
    $form_state['rtid']=$edit['rtid'];
  }

  $form['name'] = array(
    '#title' => t('Relation name'),
    '#type' => 'textfield',
    '#size' => 35,
    '#default_value' => $edit['name'],
    '#required' => TRUE,
  );
  $form['name_p'] = array(
    '#title' => t('Relation name - plural form'),
    '#type' => 'textfield',
    '#size' => 35,
    '#default_value' => $edit['name_p'],
    '#required' => TRUE,
  );
  $form['oneway'] = array(
    '#title' => t('One way relation'),
    '#type' => 'checkbox',
    '#default_value' => $edit['oneway'],
    '#required' => FALSE,
  );
  $form['active'] = array(
    '#title' => t('Active'),
    '#type' => 'checkbox',
    '#default_value' => $edit['active'],
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function friendlist_ui_form_relation_type_submit($form, &$form_state){

  $values=$form_state['values'];

  // It's a new relation type: save
  if( $form_state['is_new'] ){
    $r = friendlist_api_create_relation_type($values['name'], $values['name_p'], $values['oneway'], $values['active']);
   drupal_set_message( t("Relation type created"));
  // It's an existing relation type: edit
  } else {
    $r = friendlist_api_edit_relation_type($form_state['rtid'], $values['name'], $values['name_p'], $values['oneway'], $values['active']);
   drupal_set_message( t("Relation type saved"));
  }
  $form_state['redirect'] = 'admin/settings/friendlist_relation_types';
}

function friendlist_ui_relation_edit($rtid){

  friendlist_api_load_relation_type($rtid,$return);
  return drupal_get_form('friendlist_ui_form_relation_type',$return);
}

