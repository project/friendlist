
Module friendlist
Written by Mark Howell 2004

History:
This module started out as a drupal4.4 update for the 4.3 buddylist module.  
Since then, it has taken on several new features.  The goal that I'm heading 
for is a social network tool.  The only major features that this module is 
lacking are the "n-degrees of separation" reports, which is the most useful 
output that this could be creating.

Soon, this will be a useful networking tool.  For now, "friend of a friend" 
will be the extent to which it is useful.

TODO:
build a user hook that lists how the current user is related to the user who's profile is being viewed.
Restrict access to users' profile content based on the degrees of separation the viewee specifies.

